import unittest
import intercom

class FlattenTestCase(unittest.TestCase):
    def test_invalid_type_none(self):
        self.assertRaises(intercom.InvalidType, intercom.flatten, None)

    def test_empty_list(self):
        self.assertRaises(intercom.EmptyList, intercom.flatten, [])

    def test_non_integer_values_in_list_a(self):
        self.assertRaises(
            intercom.InvalidType, intercom.flatten, [1,[2,3],"4"]
        )

    def test_non_integer_values_in_list_b(self):
        self.assertRaises(
            intercom.InvalidType, intercom.flatten, [1,[2,3],None]
        )

    def test_non_integer_values_in_list_c(self):
        self.assertRaises(
            intercom.InvalidType, intercom.flatten, [1,[2,3],4.2]
        )

    def test_flatten_list(self):
        self.failUnlessEqual(
            [1,2,3,4,5,6], intercom.flatten([1,[2,[3,4]],5,[6]])
        )

class LocateCustomersTestCase(unittest.TestCase):
    #Begin testing the __init__ method for the value types of origin_latitude
    def test_origin_latitude_none(self):
        self.assertRaises(
            intercom.InvalidType, intercom.LocateCustomers,
            origin_latitude=None
        )

    def test_origin_latitude_int(self):
        self.assertRaises(
            intercom.InvalidType, intercom.LocateCustomers,
            origin_latitude=1234567
        )

    def test_origin_latitude_invalid_str(self):
        self.assertRaises(
            intercom.InvalidType, intercom.LocateCustomers,
            origin_latitude="FOO"
        )

    def test_origin_latitude_valid_str(self):
        customer = intercom.LocateCustomers(origin_latitude="53.3381985")
        self.assertIsInstance(customer.origin_latitude, float)

    def test_origin_latitude_float(self):
        customer = intercom.LocateCustomers(origin_latitude=53.3381985)
        self.assertIsInstance(customer.origin_latitude, float)

    #Begin testing the __init__ method for the value types of origin_longitude
    def test_origin_longitude_none(self):
        self.assertRaises(
            intercom.InvalidType, intercom.LocateCustomers,
            origin_longitude=None
        )

    def test_origin_longitude_int(self):
        self.assertRaises(
            intercom.InvalidType, intercom.LocateCustomers,
            origin_longitude=1234567
        )

    def test_origin_longitude_invalid_str(self):
        self.assertRaises(
            intercom.InvalidType, intercom.LocateCustomers,
            origin_longitude="FOO"
        )

    def test_origin_longitude_valid_str(self):
        customer = intercom.LocateCustomers(origin_longitude="-6.2592576")
        self.assertIsInstance(customer.origin_latitude, float)

    def test_origin_longitude_float(self):
        customer = intercom.LocateCustomers(origin_longitude=-6.2592576)
        self.assertIsInstance(customer.origin_longitude, float)

    #Begin testing the _open_file method
    def test_open_non_existant_file(self):
        customer = (
            intercom.LocateCustomers(
                customers_file="./test_foo_bar_does_not_exist.txt"
            )
        )
        self.assertRaises(intercom.FileDoesNotExist, customer._open_file)

    def test_open_file_that_is_a_directory(self):
        customer = (
            intercom.LocateCustomers(customers_file="./test_foo_bar")
        )
        self.assertRaises(intercom.IsADirectoryNotAFile, customer._open_file)

    def test_open_file(self):
        customer = intercom.LocateCustomers(customers_file="./customers.json")
        list_of_customers = customer._open_file()
        self.assertIsInstance(list_of_customers, list)

    # Testing the conversion to a dictionary from a list of strings
    def test_invalid_json_during_eval(self):
        customer = (
            intercom.LocateCustomers(
                customers_file="./invalid_customers.json"
            )
        )
        list_of_customers = customer._open_file()
        self.assertRaises(
            intercom.InvalidJson,
            customer._convert_to_dict, list_of_customers
        )

    def test_invalid_list_during_eval(self):
        customer = intercom.LocateCustomers()
        self.assertRaises(
            intercom.InvalidType,
            customer._convert_to_dict, None
        )

    def test_fail_if_already_in_dictionary(self):
        customer = intercom.LocateCustomers()
        self.assertRaises(
            intercom.InvalidType,
            customer._convert_to_dict, [{}]
        )

    def test_convert_into_dictionary(self):
        customer = intercom.LocateCustomers()
        list_of_customers = (
            customer._convert_to_dict(customer._open_file())
        )
        self.assertIsInstance(list_of_customers[0], dict)

    # Testing the sorting of a key in a dictionary inside of a list
    def test_sorting_with_invalid_key(self):
        customer = intercom.LocateCustomers()
        list_of_customers = (
            customer._convert_to_dict(customer._open_file())
        )
        self.assertRaises(
            intercom.InvalidKey,
            customer._sort_list, list_of_customers, "foo_bar"
        )

    def test_sorting_with_valid_key(self):
        customer = intercom.LocateCustomers()
        list_of_customers = (
            customer._convert_to_dict(customer._open_file())
        )
        customer._sort_list(list_of_customers, by="user_id")
        self.assertEqual(
            list_of_customers[0].get("user_id"), 1
        )

    # Testing the get_distance method.
    def test_get_distance_invalid_bool(self):
        customer = intercom.LocateCustomers()
        list_of_customers = (
            customer._convert_to_dict(customer._open_file())
        )
        customer._sort_list(list_of_customers)
        latitude = list_of_customers[0].get('latitude')
        longitude = list_of_customers[0].get('longitude')
        self.assertRaises(
            intercom.InvalidType,
            customer.get_distance, latitude, longitude, kilometers="FOO"
        )

    def test_get_distance_in_kilometers(self):
        customer = intercom.LocateCustomers()
        list_of_customers = (
            customer._convert_to_dict(customer._open_file())
        )
        customer._sort_list(list_of_customers)
        latitude = list_of_customers[0].get('latitude')
        longitude = list_of_customers[0].get('longitude')
        distance = customer.get_distance(latitude, longitude)
        self.assertEqual(distance, 313.195799206986)

    def test_get_distance_in_miles(self):
        customer = intercom.LocateCustomers()
        list_of_customers = (
            customer._convert_to_dict(customer._open_file())
        )
        customer._sort_list(list_of_customers)
        latitude = list_of_customers[0].get('latitude')
        longitude = list_of_customers[0].get('longitude')
        distance = customer.get_distance(latitude, longitude, False)
        self.assertEqual(distance, 194.61091555933854)


    # Testing the within_distance method.
    def test_within_distance_100_kilometers_a(self):
        customer = intercom.LocateCustomers()
        customers_within_100_kilometers = (
            customer.within_distance(
                distance=100, kilometers=True, sort_by="user_id"
            )
        )
        self.assertEqual(len(customers_within_100_kilometers), 16)

    def test_within_distance_100_kilometers_b(self):
        kilometers_under_100 = [
            10.44810474468498, 23.163770558335244, 23.952694622356724,
            83.70122058262831, 38.04620869919844, 41.68992239167155,
            62.0843970995196, 43.57314686766388, 96.26607986256003,
            82.84855417829647, 89.18108248103488, 98.76067392735528,
            72.22829278196673, 82.66073409297282, 44.14671525763004,
            38.21291826216186
        ]
        customer = intercom.LocateCustomers()
        customers_within_100_kilometers = (
            customer.within_distance(
                distance=100, kilometers=True, sort_by="user_id"
            )
        )
        kilometers_only = (
            map(
                lambda x: x['distance_from_origin'],
                customers_within_100_kilometers
            )
        )
        self.assertListEqual(kilometers_under_100, kilometers_only)

    def test_within_distance_100_miles_a(self):
        customer = intercom.LocateCustomers()
        customers_within_100_kilometers = (
            customer.within_distance(
                distance=100, kilometers=False, sort_by="user_id"
            )
        )
        self.assertEqual(len(customers_within_100_kilometers), 20)

    def test_within_distance_100_miles_b(self):
        miles_under_100 = [
            6.492153583705086, 14.393304787542379, 14.883519646090166,
            52.00954550560303, 82.72633412503271, 81.51960056420394,
            23.640826368872716, 25.90492588592803, 38.57746940437747,
            27.075107735124583, 59.816989840850106, 51.479722979139176,
            55.41457502352081, 61.36705927386269, 94.19054024445515,
            67.9685309113711, 44.880596173950764, 51.36301694777536,
            27.431506734695585, 23.744414925178244
        ]
        customer = intercom.LocateCustomers()
        customers_within_100_miles = (
            customer.within_distance(
                distance=100, kilometers=False, sort_by="user_id"
            )
        )
        miles_only = (
            map(
                lambda x: x['distance_from_origin'],
                customers_within_100_miles
            )
        )
        self.assertListEqual(miles_under_100, miles_only)


if __name__ == "__main__":
    unittest.main()
