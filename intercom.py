#!/usr/bin/env python
from math import cos, acos, sin, pi
import os

class InvalidType(Exception):
    def __init__(self, message, values, value_type):
        super(InvalidType, self).__init__(message)
        self.values = values
        self.value_type = value_type

class EmptyList(Exception):
    pass

class InvalidDepth(Exception):
    pass

class InvalidKey(InvalidType):
    pass

class InvalidJson(InvalidType):
    pass

class FileDoesNotExist(Exception):
    pass

class PermissionDenied(IOError):
    pass

class IsADirectoryNotAFile(IOError):
    pass

valid_iter_types = (list,tuple)

def flatten(unflatten_list):
    """Flatten a multidimension array
    Args:
        unflatten_list (list|tuple): Example..
            [[1,2,[3]],[5,[6,7]],4]

    Basic Usage:
        >>> from intercom import flatten
        >>> needs_flattening = [[1,2,[3]],[5,[6,7]],4]
        >>> flattend_list = flatten(needs_flattening)
        >>> print flattend_list
        [1, 2, 3, 5, 6, 7, 4]

    Returns:
        List
    """
    flatten_list = []
    if (not flatten_list and unflatten_list and
            isinstance(unflatten_list, valid_iter_types)):
        for entry in unflatten_list:
            if not isinstance(entry, valid_iter_types):
                if isinstance(entry, int):
                    flatten_list.append(entry)

                else:
                    message = "Not a valid integer {0}".format(entry)
                    raise InvalidType(message, entry, type(entry))

            else:
                flatten_list += flatten(entry)

    elif isinstance(unflatten_list, valid_iter_types) and not unflatten_list:
        message = "Empty list was passed"
        raise EmptyList(message)

    elif not isinstance(unflatten_list, valid_iter_types):
        message = "Not a valid list or tuple"
        raise InvalidType(message, unflatten_list, type(unflatten_list))

    return flatten_list


class LocateCustomers(object):
    """Locate customers closest to the base coordinates.

    Attributes:
        origin_latitude (float): The latitude coordinates or origin.
        origin_longitude (float): The longitude coordinates or origin.
        customers_file (str): The full path to the file that contains
            the customer information in proper json format.
        kilometers (int): Sphere of radius 6373 kilometers
        miles (int): Sphere of radius 3960 miles
    """
    def __init__(self, origin_latitude=53.3381985,
                 origin_longitude=-6.2592576,
                 customers_file="./customers.json"):
        """
        Kwargs:
            origin_latitude (float): The latitude coordinates or origin.
            origin_longitude (float): The longitude coordinates or origin.
            customers_file (str): The full path to the file that contains
                the customer information in proper json format.

        Basic Usage:
            >>> from intercom import LocateCustomers
            >>> lat = 53.3381985
            >>> lon = -6.2592576
            >>> customers_file = "./customers.json"
            >>> customers = LocateCustomers(lat, lon, customers_file)
        """
        if not isinstance(origin_latitude, float):
            origin_latitude = self._convert_to_float(origin_latitude)

        if not isinstance(origin_longitude, float):
            origin_longitude = self._convert_to_float(origin_longitude)

        self.origin_latitude = origin_latitude
        self.origin_longitude = origin_longitude
        self.customers_file = customers_file
        self.kilometers = 6373
        self.miles = 3960

    def _convert_to_float(self, value):
        """Convert a valid string, unicode string, or an integer into a float.
        Args:
            value (str|unicode): A valid representation of a string or an
                integer that can be converted into a float.conjugate

        Basic Usage:
            >>> from intercom import LocateCustomers
            >>> customer = LocateCustomers()
            >>> customer._convert_to_float("12345.678")
            12345.678

        """
        if isinstance(value, (str, unicode)):
            try:
                converted_value = float(value)
            except ValueError:
                message = "Can not convert {0} to a float".format(value)
                raise InvalidType(message, value, type(value))
            return converted_value
        else:
            message = (
                "Can only convert an integer or a string, not a {0}"
                .format(type(value))
            )
            raise InvalidType(message, value, type(value))

    def _convert_to_dict(self, list_of_customers):
        """Convert each element in the array to a valid python dictionary.
        Args:
            list_of_customers (list): This is a list of strings that can be
                evaluated into a dictionary. Example...
            [
                '{
                    "latitude": "52.986375",
                    "user_id": 12,
                    "name": "Christina McArdle",
                    "longitude": "-6.043701"
                }\n',
            ]

        Basic Usage:
            >>> from intercom import LocateCustomers
            >>> latitude = 53.3381985
            >>> longitude = -6.2592576
            >>> customers = LocateCustomers(latitude, longitude)
            >>> list_of_customers = customers.open_file("customers.json")
            >>> list_of_customers = (
                customers._convert_to_dict(list_of_customers)
            )

        Returns:
            List of dictionaries
        """
        converted_list = []
        base_error_message = "Invalid json: {0}"
        if isinstance(list_of_customers, tuple):
            list_of_customers = list(list_of_customers)

        if isinstance(list_of_customers, list):
            for customer in list_of_customers:
                if isinstance(customer, (str, unicode)):
                    try:
                        converted = eval(customer)
                    except SyntaxError:
                        message = base_error_message.format(customer)
                        raise InvalidJson(message, customer, type(customer))
                    if isinstance(converted, dict):
                        converted_list.append(converted)
                    else:
                        message = base_error_message.format(converted)
                        raise InvalidJson(message, converted, type(converted))
                else:
                    message = "{0} not equal to a string".format(customer)
                    raise InvalidType(message, customer, type(customer))
        else:
            message = "{0} not equal to a list".format(list_of_customers)
            raise InvalidType(
                message, list_of_customers, type(list_of_customers)
            )

        return converted_list

    def _sort_list(self, list_of_customers, by="user_id"):
        """Sort list inline by key. The default key is user_id.
        Args:
            list_of_customers (list): List of dictionaries.

        Kwargs:
            by (str): Sort by a valid string in the dictionary.

        Basic Usage:
            >>> from intercom import LocateCustomers
            >>> latitude = 53.3381985
            >>> longitude = -6.2592576
            >>> customers = LocateCustomers(latitude, longitude)
            >>> list_of_customers = customers.open_file("customers.json")
            >>> list_of_customers = (
                customers._convert_to_dict(list_of_customers)
            )
            >>> customers._sort_list(list_of_customers, by="user_id")
        """
        if list_of_customers and isinstance(list_of_customers, list):
            if list_of_customers[0].get(by, None):
                list_of_customers.sort(key=lambda key: key[by])
            else:
                raise InvalidKey(
                    "Key does not exist: {0}".format(by), by, type(by)
                )
        elif not isinstance(list_of_customers, list):
            message = "Invalid type {0}".format(type(list_of_customers))
            raise InvalidType(
                message, list_of_customers, type(list_of_customers)
            )
        else:
            message = "Empty list was passed"
            raise EmptyList(message)

    def _open_file(self):
        """Open the customers file and return it back in a list.

        Basic Usage:
            >>> from intercom import LocateCustomers
            >>> latitude = 53.3381985
            >>> longitude = -6.2592576
            >>> customers = (
                LocateCustomers(latitude, longitude, "./customers.json")
            )
            >>> list_of_customers = customers._open_file()
            [
                '{
                    "latitude": "52.986375",
                    "user_id": 12,
                    "name": "Christina McArdle",
                    "longitude": "-6.043701"
                }\n'
            ]

        Returns:
            List of strings
        """
        list_of_customers = []
        if (os.path.exists(self.customers_file) and
                os.path.isfile(self.customers_file)):
            try:
                list_of_customers = open(self.customers_file, "r").readlines()
            except IOError as error:
                if error.errno == 13:
                    raise PermissionDenied(
                        13, "Insufficient Permissions", self.customers_file
                    )
        elif os.path.isdir(self.customers_file):
            message = (
                "{0} is a directory and not a file"
                .format(self.customers_file)
            )
            raise IsADirectoryNotAFile(21, message, self.customers_file)

        else:
            raise FileDoesNotExist(
                "{0} does not exist".format(self.customers_file)
            )

        return list_of_customers

    def within_distance(self, distance=100,
                        kilometers=True, sort_by="user_id"):
        """Locate customers within x kilometers or miles from the
            home latitude and home longitude.

        Kwargs:
            distance (int): The maximum distance to search up to.
                default=100
            kilometers (bool): Search by kilometers if True. Search by miles
                If False.
                default=True
            sort_by (str): The field to sort on.
                default="user_id"

        Basic Usage:
            >>> from intercom import LocateCustomers
            >>> latitude = 53.3381985
            >>> longitude = -6.2592576
            >>> customers = (
                LocateCustomers(latitude, longitude, "customers_file")
            )
            >>> customers_within_distance = (
                customers.within_distance(distance=100, sort_by="user_id")
            )
            >>> print json.dumps(customers_within_distance, indent=4)
            [
                {
                    "latitude": 53.2451022,
                    "user_id": 4,
                    "name": "Ian Kehoe",
                    "longitude": -6.238335,
                    "distance_from_origin": 10.44810474468498
                },
                {
                    "latitude": 53.1302756,
                    "user_id": 5,
                    "name": "Nora Dempsey",
                    "longitude": -6.2397222,
                    "distance_from_origin": 23.163770558335244
                }
            ]

        Returns:
            List of dictionaries
        """
        list_of_customers = self._convert_to_dict(self._open_file())
        list_within_distance = []

        if not isinstance(kilometers, bool):
            message = "{0} is not of type bool".format(kilometers)
            raise InvalidType(message, kilometers, type(kilometers))

        if kilometers:
            distance_metric = "kilometers"
        else:
            distance_metric = "miles"

        for x in xrange(len(list_of_customers)):
            if (list_of_customers[x].get("latitude", None) and
                    list_of_customers[x].get("longitude", None)):

                dest_lat = list_of_customers[x]["latitude"]
                dest_lon = list_of_customers[x]["longitude"]
                if isinstance(list_of_customers[x]["latitude"], str):
                    dest_lat = float(dest_lat)

                if isinstance(list_of_customers[x]["longitude"], str):
                    dest_lon = float(dest_lon)

                list_of_customers[x]["latitude"] = dest_lat
                list_of_customers[x]["longitude"] = dest_lon
                list_of_customers[x]["distance_metric"] = distance_metric
                list_of_customers[x]['distance_from_origin'] = (
                    self.get_distance(dest_lat, dest_lon, kilometers)
                )

                if list_of_customers[x]['distance_from_origin'] <= distance:
                    list_within_distance.append(list_of_customers[x])
            else:
                raise InvalidKey(
                    "Key does not exist latitude or longitude"
                     "latitude or longitude", "string"
                )

        if list_within_distance:
            self._sort_list(list_within_distance, by=sort_by)

        return list_within_distance

    def get_distance(self, destination_lat, destination_lon, kilometers=True):
        """Get the distance from the base coordinates to the
           destination coordinates and returns it in kilometers or miles.

        Args:
            destination_lat (float): The destination latitude as a float.
            destination_lon (float): The destination longitude as a float.

        Kwargs:
            kilometers (bool): If kilometers is True, then return the
                distance in kilometers. If False, than return the distance
                in miles.

        Basic Usage:
            >>> from intercom import LocateCustomers
            >>> origin_latitude = 25.8151385
            >>> origin_longitude = -80.3816713
            >>> dest_latitude = 25.8115175
            >>> dest_longitude = -80.382745
            >>> customer = LocateCustomers(origin_latitude, origin_longitude)
            >>> kilometers_from_origin = (
                customer.get_distance(dest_latitude, dest_longitude)
            )
            >>> print kilometers_from_origin
            0.2590282422171131

        Returns:
            Float of the distance
        """
        # For my example I used the coordinates from my house to the
        # nearest Starbucks.

        # This site http://www.gcmap.com/faq/gccalc, really simplified what
        # I needed to do in order to compute the great circle distance.

        # Convert latitude and longitude to spherical coordinates in radians.
        # 3.141592653589793 / 180 = 0.017453292519943295
        if not isinstance(destination_lat, float):
            destination_lat = self._convert_to_float(destination_lat)

        if not isinstance(destination_lon, float):
            destination_lon = self._convert_to_float(destination_lon)

        if not isinstance(kilometers, bool):
            message = "{0} is not of type bool".format(kilometers)
            raise InvalidType(message, kilometers, type(kilometers))

        degrees_to_radians = pi / 180.0

        origin_lat_in_radians = self.origin_latitude * degrees_to_radians
        dest_lat_in_radians = destination_lat * degrees_to_radians

        origin_lon_in_radians  = self.origin_longitude * degrees_to_radians
        dest_lon_in_radians  = destination_lon * degrees_to_radians

        theta = dest_lon_in_radians - origin_lon_in_radians

        # The distance before multiplying against against
        dist = (
            acos(
                sin(origin_lat_in_radians) * sin(dest_lat_in_radians) +
                cos(origin_lat_in_radians) * cos(dest_lat_in_radians) *
                cos(theta)
            )
        )

        if dist < 0:
            dist = dist + pi

        if kilometers:
            return dist * self.kilometers
        else:
            return dist * self.miles

        return dist
