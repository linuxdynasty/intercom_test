# README #

I created this repository just for the intercom programming test. This includes the flatten example and the locate customers within 100 kilometers from the office.
I did go a little above the requirements of the test, but I thought of some added features, that should just go quite nice with the current requirements.

### Requirements ###
1. Write some code, that will flatten an array of arbitrarily nested arrays of integers into a flat array of integers. e.g. [[1,2,[3]],4] -> [1,2,3,4]. 
2. Write a program that will read the full list of customers and output the names and user ids of matching customers (within 100km), sorted by user id (ascending). 

### What I did for flatten ###
1. The flatten function does exactly what intercom.io requested and nothing more.

### What I did for locate customers within 100 kilometers ###
1. Locate customers within 1 kilometer or 1000 kilometers.
2. Locate customers within miles instead of kilometers or vice versa.
3. Sort by user_id, name, distance_from_origin.
4. Only output name and user_id or any of the available fields such as [user_id, distance_from_origin, longitude, latitude, distance_metric, name]

### Step 1, clone this repository ###
1. git clone https://linuxdynasty@bitbucket.org/linuxdynasty/intercom_test.git
2. cd intercom_test

### Step 2, run the test suite ###
In your favorite shell.
```
#!bash
python intercom-command run_test_suite
```
OR
```
#!bash
python -m unittest discover -v
```
Or in your favorite python console.
```
#!python
>>> import unittest
>>> testsuite = unittest.TestLoader().discover('.')
>>> unittest.TextTestRunner(verbosity=2).run(testsuite)
```

### Step 3, try out flatten ###
In your favorite shell.
```
#!bash
python intercom-command flatten --array="[1,[2,3,[4,5]],6,7]"
```
Or in your favorite python console.
```
#!python
>>> from intercom import flatten
>>> print flatten([1,[2,3,[4,5]],6,7])
[1, 2, 3, 4, 5, 6, 7]
```

### Step 4, try out LocateCustomers ###
In your favorite shell. If no options are passed, than it will use the defaults.
```
#!bash
python intercom-command locate_customers
```
Or in your favorite python console.
```
#!python
>>> from intercom import LocateCustomers
>>> locate = LocateCustomers()
>>> locate.within_distance(distance=100, sort_by="user_id")
```

### Step 5, How to use intercom-command ###

## Run the command with just -h ##
```
#!bash
$ python intercom-command -h
usage: python intercom-command <command> -h

The Intercom command line tool.
These are the various commands you can call
    * flatten
    * locate_customers
    * run_test_suite
    

optional arguments:
  -h, --help            show this help message and exit

Choose a valid command:
  {flatten,locate_customers,run_test_suite}
                        The command you want to run
```
## Run the command with the flatten option and -h ##
```
#!bash
$ python intercom-command flatten -h
usage: python intercom-command flatten --array="[<multidimesional_array>]" 

optional arguments:
  -h, --help     show this help message and exit
  --array ARRAY  Flatten a multidimesional array, example.. "[1,[2,3],4]"
```
## Run the command with the locate_customers option and -h ##
```
#!bash
$ python intercom-command locate_customers -h
usage: 
python intercom-command locate_customers --origin_lat=<latitude> --origin_lon=<longitude> --customers_json_file=<json_file> --distance=<distance>
Examples:
    python intercom-command locate_customers --origin_lat=53.3381985 --origin_lon=-6.2592576 --customers_json_file="./customers.json" --distance=100
    python intercom-command locate_customers --origin_lat=53.3381985 --origin_lon=-6.2592576 --customers_json_file="./customers.json" --distance=100 --fields=user_id --fields=name --fields=distance_from_origin

Examples using the provided defaults:
    python intercom-command locate_customers

Use miles instead of kilometers:
    python intercom-command locate_customers --miles

Print all of the fields instead of the default user_id and name:
    python intercom-command locate_customers --fields=user_id --fields=name --fields=distance_from_origin --fields=latitude --fields=longitude

optional arguments:
  -h, --help            show this help message and exit
  --origin_lat ORIGIN_LAT
                        The origin latitude to base your search off of.
                        (default: 53.3381985)
  --origin_lon ORIGIN_LON
                        The origin longitude to base your search off of.
                        (default: -6.2592576)
  --customers_json_file CUSTOMERS_FILE
                        The origin longitude to base your search off of.
                        (default: ./customers.json)
  --distance DISTANCE   Distance from the office do you want to invite
                        customers from. (default: 100)
  --miles               Search within miles. The default is to use kilometers
                        (default: False)
  --sort_by {user_id,distance_from_origin,name}
                        The fields you want to sort by (default: user_id)
  --fields {user_id,distance_from_origin,longitude,latitude,distance_metric,name}
                        The fields you want to output in the report (default:
                        ['user_id', 'name'])
```
